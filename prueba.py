from clima.geoapi import GeoAPI



print ('1 -----------')
print ("¿Hace calor en Pehuajó?")
print ( GeoAPI.is_hot_in_pehuajo () )
 


print ('2 -----------')
from grilla.pandas import *

print (productos ())
print ("Limon"         , 5, is_product_available("Limon"         , 5) )
print ("Chocolate"     , 3, is_product_available("Chocolate"     , 3) )
print ("Chocolate"     , 4, is_product_available("Chocolate"     , 4) )
print ("Dulce de leche", 5, is_product_available("Dulce de Leche", 5) )
print ("Frutilla      ", 5, is_product_available("Frutilla      ", 5) )




print ('3 -----------')

from robot.funciones import *


"""
Ejemplo:
"primavera2021" deberia devolver True, ya que al compararlo:
vs "Primavera2021" = 2 caracteres de diferencia ("p" y "P")
vs "Verano2021" = 7 caracteres de diferencia ('i', 'n', 'o', 'm', 'V',
'p', 'v')
vs "Navidad2x1" = 8 caracteres de diferencia ('N', 'm', '0', 'x', 'e',
'd', 'p', 'r')
vs "heladoFrozen" = 14 caracteres de diferencia ('z', 'i', 'v', 'n',
'o', 'm', '2', '0', 'd', 'p', '1',

"""

print ("primavera2021", diferencia ("primavera2021", "Primavera2021") ) # 2
print ("Verano2021"   , diferencia ("Verano2021"   , "Primavera2021") ) # 7 
print ("Navidad2x1"   , diferencia ("Navidad2x1"   , "Primavera2021") ) # 8
print ("heladoFrozen" , diferencia ("heladoFrozen" , "Primavera2021") ) # 14

print ( "código primavera2021", validate_discount_code ("primavera2021") ) #True
