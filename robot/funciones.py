_AVAILABLE_DISCOUNT_CODES = ["Primavera2021"
                            , "Verano2021"
                            , "Navidad2x1"
                            ,"heladoFrozen"
                            ]

def descuentos_disponibles ():
    return _AVAILABLE_DISCOUNT_CODES


def validate_discount_code(codigo_descuento):
    for codigo in descuentos_disponibles ():
        if diferencia (codigo_descuento, codigo) < 3:
            return True
    return False


"""
Compara dos cadenas y devuelve la cantidad de caracteres diferentes, no cuenta repeticiones.
@param palabra1: cadena, primer palabra a comparar.
@param palabra2: cadena, segunda palabra a comparar.
@return: entero
"""
def diferencia (palabra1, palabra2):
    contador = 0
    letra_contada = []
    for letra in palabra1:
        if letra not in letra_contada:
            if letra not in palabra2:
                contador = contador +1
                letra_contada.append (letra)

    for letra in palabra2:
        if letra not in letra_contada:
            if letra not in palabra1:
                contador = contador +1
                letra_contada.append (letra)

    return contador
            





