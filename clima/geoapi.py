
from clima.owm import Openweathermap

class GeoAPI (Openweathermap):
    
    LAT = "-35.836948753554054"
    LON = "-61.870523905384076"
    
    temperatura_calor = 28

    @classmethod
    def is_hot_in_pehuajo(cls):

        
        temperatura = cls.obtener_temperatura (GeoAPI.LAT, GeoAPI.LON)

        if cls.error == False:
            return temperatura > cls.temperatura_calor
        
        return False
        