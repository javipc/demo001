


"""

    Conecta con Open Weather Map

"""

import requests


class Openweathermap:
    API_KEY = "d81015613923e3e435231f2740d5610b"    

    URL = "https://api.openweathermap.org/data/2.5/weather"

    error = False

    """
    Conecta con la API y obtiene lo datos del clima.
    @param latitud: número.
    @param longitud: número.
    @return: json
    """

    @classmethod
    def obtener_datos (clase, latitud, longitud):
        direccion = clase.URL + f"?units=metric&lat={latitud}&lon={longitud}&appid={clase.API_KEY}"
        clase.error = False
        try:
            respuesta = requests.api.get (direccion)
            return respuesta.json ()
        except:
            clase.error = True            
        return {}


    """
    Devuelve la temperatura actual en una determinada posición.
    @param latitud : número, posición.
    @param longitud: número, posición.
    @return: número.
    """

    @classmethod
    def obtener_temperatura (clase, latitud, longitud):
        
        clima = clase.obtener_datos (latitud, longitud)
        
        if clase.error:
            return None
        try:
            return int (clima ['main']['temp'])
        except:
            return None

    