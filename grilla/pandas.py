import pandas as pd


_PRODUCT_DF = pd.DataFrame({"product_name": ["Chocolate", "Granizado", "Limon", "Dulce de Leche"], "quantity": [3,10,0,5]})


def productos ():
    return _PRODUCT_DF

def is_product_available(nombre_producto, cantidad):
    # Obtiene el parámetro para buscar por nombre_producto.
    busqueda = _PRODUCT_DF ['product_name'] == nombre_producto
    
    # Busca el producto.
    producto = _PRODUCT_DF [busqueda]

    # Devuelve la cantidad de productos encontrados.
    if producto ['quantity'].values.size > 0:        
        return producto ['quantity'].values [0] >= cantidad

    return 0


